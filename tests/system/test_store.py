import json

from models.item import ItemModel
from models.store import StoreModel
from tests.base_test import BaseTest


class TestStore(BaseTest):
    def test_create_store(self):
        with self.app() as client:
            with self.app_context():
                response = client.post('/store/test store')

                self.assertEqual(response.status_code, 201)
                self.assertIsNotNone(StoreModel.find_by_name('test store'))
                self.assertDictEqual(json.loads(response.data),
                                     {'name': 'test store', 'items': []})

    def test_create_store_duplicate(self):
        with self.app() as client:
            with self.app_context():
                client.post('/store/test store')
                response = client.post('/store/test store')

                self.assertEqual(response.status_code, 400)
                self.assertDictEqual(json.loads(response.data),
                                     {'message': f"A store with name 'test store' already exists."})

    def test_delete_store(self):
        with self.app() as client:
            with self.app_context():

                self.assertIsNone(StoreModel.find_by_name('test store'))
                client.post('/store/test store')
                self.assertIsNotNone(StoreModel.find_by_name('test store'))

                response = client.delete('/store/test store')
                self.assertDictEqual(json.loads(response.data), {'message': "Store 'test store' deleted"})
                self.assertIsNone(StoreModel.find_by_name('test store'))

    def test_return_store(self):
        with self.app() as client:
            with self.app_context():
                client.post('/store/test store')

                response = client.get('/store/test store')
                self.assertEqual(response.status_code, 200)
                self.assertDictEqual(json.loads(response.data),
                                     {'name': 'test store', 'items': []})

    def test_return_store_empty(self):
        with self.app() as client:
            with self.app_context():

                response = client.get('/store/test store')
                self.assertEqual(response.status_code, 404)
                self.assertDictEqual(json.loads(response.data),
                                     {'message': 'Store not found'})

    def test_list_stores(self):
        with self.app() as client:
            with self.app_context():
                client.post('/store/test store')

                response = client.get('/stores')
                self.assertDictEqual(json.loads(response.data),
                                     {'stores': [{'name': 'test store', 'items': []}]})

    def test_list_stores_with_items(self):
        with self.app() as client:
            with self.app_context():
                StoreModel('test store').save_to_db()
                ItemModel('test item', 9.99, 1).save_to_db()

                response = client.get('/stores')
                self.assertDictEqual(json.loads(response.data),
                                     {'stores': [{'name': 'test store',
                                                  'items': [{'name': 'test item',
                                                             'price': 9.99}]}]})
