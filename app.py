import os

from flask import Flask, jsonify
from flask_restful import Api
from flask_jwt import JWT, JWTError

from resources.item import Item, ItemList
from resources.store import Store, StoreList
from resources.user import UserRegister
from security import authenticate, identity

app = Flask(__name__)

app.config['DEBUG'] = True

app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL', 'sqlite:///data.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# This is needed in order to suppress JWT authorization exception when running test cases
app.config['PROPAGATE_EXCEPTIONS'] = True
# This is required for JWT to work
app.secret_key = 'secret123'
api = Api(app)

# ---------------------------------------------------------------------------------------------
# In its compact form, JSON Web Tokens consist of three parts separated by dots (.), which are:
# Header, Payload, Signature i.e. xxxxx.yyyyy.zzzzz
# ---------------------------------------------------------------------------------------------
# jwt object takes in the app along with authenticate and identity functions
# The jwt object is used in order to call the /auth endpoint
jwt = JWT(app, authenticate, identity)

api.add_resource(Store, '/store/<string:name>')
api.add_resource(Item, '/item/<string:name>')
api.add_resource(ItemList, '/items')
api.add_resource(StoreList, '/stores')

api.add_resource(UserRegister, '/register')


@app.errorhandler(JWTError)
def auth_error_handler(err):
    return jsonify({'message': 'Could not authorize. Ensure to include a valid Authorization header.'}), 401


if __name__ == '__main__':
    from db import db

    db.init_app(app)

    if app.config['DEBUG']:
        @app.before_first_request
        def create_tables():
            db.create_all()

    app.run(port=5000, debug=True)
