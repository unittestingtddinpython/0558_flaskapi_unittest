"""
This test is basically against the only resource that exists for user: POST.
It should be tested if new user is created unless he already exists.

NOTE: Related to users there is also the JWT from /auth endpoint, which needs
to be received back once a new user is being created.

"""
import json

from models.user import UserModel
from tests.base_test import BaseTest


class UserTest(BaseTest):
    def test_register_user(self):
        # client is needed for mocking api requests
        # db context is needed, too
        with self.app() as client:
            with self.app_context():
                response = client.post('/register', data={'username': 'test user',
                                                          'password': 'secretpass'})

                self.assertEqual(response.status_code, 201)
                self.assertIsNotNone(UserModel.find_by_username('test user'))
                # Note that json object (here: json string) needs to be converted to python dictionary
                # json.load(s) --> load string does this job
                self.assertDictEqual(json.loads(response.data),
                                     {'message': f"User 'test user' successfully created."})

    def test_register_and_login(self):
        with self.app() as client:
            with self.app_context():
                client.post('/register', data={'username': 'test user',
                                               'password': 'secretpass'})
                auth_request = client.post('/auth',
                                           data=json.dumps({'username': 'test user', 'password': 'secretpass'}),
                                           headers={'Content-Type': 'application/json'})
                # NOTE: JWT returns a dictionary with an encoded token as i.e. {'access_token': 'xxxxx.yyyyy.zzzzz'}
                # The encoded content consists of a header, payload and signature [ https://jwt.io/introduction/ ]
                self.assertIn('access_token', json.loads(auth_request.data).keys())    # ['access_token']

    def test_register_duplicate_user(self):
        with self.app() as client:
            with self.app_context():
                client.post('/register', data={'username': 'test user',
                                               'password': 'secretpass'})
                response = client.post('/register', data={'username': 'test user',
                                               'password': 'secretpass'})

                self.assertDictEqual(json.loads(response.data),
                                     {'message': 'A user with that username already exists'})
                self.assertEqual(response.status_code, 400)
