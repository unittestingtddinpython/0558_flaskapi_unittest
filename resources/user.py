from flask_restful import Resource, reqparse

from models.user import UserModel


class UserRegister(Resource):
    """
    This resource allows users to register by sending a POST request with their username and password.
    """
    parser = reqparse.RequestParser()
    # by default if section is not defined:
    #   1. look in 'data'
    #   2. look in 'json'
    parser.add_argument('username',
                        type=str,
                        required=True,
                        help="This field cannot be blank.")
    parser.add_argument('password',
                        type=str,
                        required=True,
                        help="This field cannot be blank.")

    def post(self):
        data = UserRegister.parser.parse_args()

        if UserModel.find_by_username(data['username']):
            return {'message': 'A user with that username already exists'}, 400

        user = UserModel(**data)
        user.save_to_db()

        return {'message': f"User '{data['username']}' successfully created."}, 201

