from werkzeug.security import safe_str_cmp

from models.user import UserModel


def authenticate(username, password):
    """
    Function that gets called when a user calls the /auth endpoint with their username and password.
    :param username: User's username in string format
    :param password: User's un-encrypted password in string format.
    :return: A UserModel object if authentication was successful, None otherwise.
    """
    user = UserModel.find_by_username(username)
    # safe_str_cmp: compares strings in somewhat constant time. This requires that the length of at least one string
    # is known in advance.
    if user and safe_str_cmp(user.password, password):
        return user
    # NOTE: THIS IS DEFAULT FOR EVERY PYTHON FUNCTION
    return None


def identity(payload):
    """
    JSON Web Tokens (JWT)
    --------------------------------------------------------------------------------------------------------------------
    NOTE:  Once the user is logged in, each subsequent request will include the JWT, allowing the user to access routes,
     services, and resources that are permitted with that token.
    --------------------------------------------------------------------------------------------------------------------
    Function that gets called when user has already been authenticated, and Flask-JWT verified their authorization
    header is correct.
    :param payload: A dictionary with 'identity' key, which is the user id. i.e {'identity': <user id>}
    :return: A UserModel object.
    """
    user_id = payload['identity']
    return UserModel.find_by_id(user_id)
