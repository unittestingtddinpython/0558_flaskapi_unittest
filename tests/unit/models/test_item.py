from models.item import ItemModel
from tests.unit.unit_base_test import UnitBaseTest

"""
NOTE: Running these tests separately will fail for relation to StoreModel is not initialized.
Option 1: Replace TestCase with BaseTest    <-- caveat: long test duration > 20ms
Option 2: Import StoreModel     <-- caveat: unused import statement
Option 3: Create separate 'unit_test_base' were only app is imported

"""


class ItemTest(UnitBaseTest):
    def test_create_item(self):
        item = ItemModel('test item', 19.99, 1)

        self.assertEqual(item.name, 'test item',
                         "The name of the item after creation is not equal the constructor argument.")
        self.assertEqual(item.price, 19.99,
                         "The price of the item after creation is not equal the constructor argument.")
        self.assertEqual(item.store_id, 1)
        self.assertIsNone(item.store)

    def test_item_json(self):
        item = ItemModel('test item', 19.99, 1)
        expected = {'name': 'test item', 'price': 19.99}

        self.assertEqual(item.json(), expected,
                         f"The JSON output is incorrect. Received {item.json()}, expected {expected}.")
