"""
BaseTest

This class should be the parent class to each non-unit test.
It allows for instantiation of the database dynamically and
makes sure that it is a new, blank database each time.

"""
from unittest import TestCase

from app import app
from db import db


class BaseTest(TestCase):
    # NOTE: RUNS ONCE FOR EVERY TEST CLASS
    @classmethod
    def setUpClass(cls):
        # Make sure database exists
        # NOTE: sqlite does not impose foreign key constraints < -- usually foreign key can only be created if matching
        # relationship exists ( sqlite does not check on this )
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
        app.config['DEBUG'] = False
        with app.app_context():
            db.init_app(app)

    # NOTE: RUNS ONCE FOR EVERY TEST METHOD WITHIN CLASS
    def setUp(self):
        # STEP 1: Initiate the context
        with app.app_context():
            db.create_all()
        # STEP 2: Get a test client along with the context
        self.app = app.test_client
        # everything that runs under app.app_context will have access to current_app
        # current_app:  Rather than referring to an app directly, you use the the current_app proxy, which points to
        #               the application handling the current activity. <-- avoiding 'circular import issues'
        self.app_context = app.app_context

    def tearDown(self):
        # Database is blank
        with app.app_context():
            db.session.remove()
            db.drop_all()
