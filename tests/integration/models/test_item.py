from models.item import ItemModel
from models.store import StoreModel
from tests.base_test import BaseTest


class ItemTest(BaseTest):
    """
    Test if the database is saving the content properly.
    NOTE: A context manager needs to be created that holds the testing db.
    """
    def test_crud(self):
        with self.app_context():
            # will automatically take id 1
            StoreModel('test store').save_to_db()
            item = ItemModel('test item', 19.99, 1)

            # Before testing if saving to database is working, ensure that the item does not exist initially
            self.assertIsNone(ItemModel.find_by_name('test item'),
                              f"Found an item with name {item.name}, but expected None.")
            item.save_to_db()
            self.assertIsNotNone(ItemModel.find_by_name('test item'))

            item.delete_from_db()
            self.assertIsNone(ItemModel.find_by_name('test item'))

    """
    Test that the relationship holds between the tables items (store_id) and stores (id)
    """
    def test_store_relationship(self):
        with self.app_context():
            store = StoreModel('test store')
            item = ItemModel('test item', 19.99, 1)

            store.save_to_db()
            item.save_to_db()

            # according to ItemModel, the store.name must be available now within item <-- relation
            self.assertEqual(item.store.name, 'test store')
