from db import db


class StoreModel(db.Model):
    __tablename__ = 'stores'

    # Will always be in relationship with foreign key set in target relation
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))

    """
    lazy vs. dynamic:   
    - In 'lazy' as soon StoreModel is initialized, its going to look up all items that have a relation to that Store. It 
        is then populating all items properties with a list of those items
    - In 'dynamic' items is becoming a query object, and in order to populate a store it needs to be queried actively as
        i.e "self.items.all()"
    The difference lies on when are the items fetched from the database: at (1.) initialization or when (2.) querying.
    """
    # NOTE: column of related tables is initialized
    items = db.relationship('ItemModel', lazy='dynamic')

    def __init__(self, name):
        self.name = name

    def json(self):
        return {'id': self.id,'name': self.name, 'items': [item.json() for item in self.items.all()]}

    @classmethod
    def find_by_name(cls, name):
        return cls.query.filter_by(name=name).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
