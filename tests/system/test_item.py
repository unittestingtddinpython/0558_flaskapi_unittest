"""
Test when a user requests an item:
    - and does not have the required authorization
    - is authorized, but item is not found, and item exists
    - is authorized, and creates an item, creates a duplicate item, and deletes an item
    - Note that PUT item can create and change an existing item

"""
import json

from models.item import ItemModel
from models.store import StoreModel
from models.user import UserModel
from tests.base_test import BaseTest


class ItemTest(BaseTest):
    # initialize authorization token before each test. Override setUp of BaseTest
    def setUp(self):
        # load client and context from initial setUp
        super(ItemTest, self).setUp()
        with self.app() as client:
            with self.app_context():
                UserModel('test user', 'secretpass').save_to_db()
                auth_resp = client.post('/auth',
                                        data=json.dumps({'username': 'test user',
                                                         'password': 'secretpass'}),
                                        headers={'Content-Type': 'application/json'})
                auth_token = json.loads(auth_resp.data)['access_token']
                self.access_token = f"JWT {auth_token}"

    def test_get_item_no_auth(self):
        with self.app() as client:
            with self.app_context():
                response = client.get('/item/test item')

                self.assertEqual(response.status_code, 401)
                self.assertDictEqual(json.loads(response.data),
                                     {"message": "Could not authorize. Ensure to include a valid Authorization header."}
                                     )

    def test_get_item_not_found(self):
        with self.app() as client:
            with self.app_context():
                response = client.get('item/test item',
                                      headers={'Authorization': self.access_token})

                self.assertEqual(response.status_code, 404)
                self.assertDictEqual(json.loads(response.data), {'message': 'Item not found'})

    def test_get_item(self):
        with self.app() as client:
            with self.app_context():
                StoreModel('test store').save_to_db()
                ItemModel('test item', 7.99, 1).save_to_db()
                response = client.get('/item/test item',
                                      headers={'Authorization': self.access_token})
                self.assertEqual(response.status_code, 200)

    def test_delete_item(self):
        with self.app() as client:
            with self.app_context():
                StoreModel('test store').save_to_db()
                ItemModel('test item', 7.99, 1).save_to_db()
                response = client.delete('/item/test item')

                self.assertEqual(response.status_code, 200)
                self.assertDictEqual(json.loads(response.data), {'message': 'Item deleted'})
                self.assertIsNone(ItemModel.find_by_name('test item'))

    def test_create_item(self):
        with self.app() as client:
            with self.app_context():
                self.assertIsNone(ItemModel.find_by_name('test item'))

                response = client.post('/item/test item',
                                       data=json.dumps({"price": 7.99, "store_id": 1}),
                                       headers={'Content-Type': 'application/json'})

                self.assertEqual(response.status_code, 201)
                self.assertIsNotNone(ItemModel.find_by_name('test item'))

    def test_create_duplicate_item(self):
        with self.app() as client:
            with self.app_context():
                ItemModel('test item', 7.99, 1).save_to_db()
                response = client.post('/item/test item',
                                       data=json.dumps({"price": 7.99, "store_id": 1}),
                                       headers={'Content-Type': 'application/json'})

                self.assertEqual(response.status_code, 400)
                self.assertDictEqual(json.loads(response.data),
                                     {'message': "An item with name 'test item' already exists."})

    def test_put_item(self):
        with self.app() as client:
            with self.app_context():
                self.assertIsNone(ItemModel.find_by_name('test item'))

                response = client.put('/item/test item',
                                      data=json.dumps({"price": 7.99, "store_id": 1}),
                                      headers={'Content-Type': 'application/json'})

                self.assertEqual(response.status_code, 200)
                self.assertIsNotNone(ItemModel.find_by_name('test item'))

    def test_put_update_item(self):
        with self.app() as client:
            with self.app_context():
                ItemModel('test item', 7.99, 1).save_to_db()
                self.assertEqual(ItemModel.find_by_name('test item').price, 7.99)

                response = client.put('/item/test item',
                                      data=json.dumps({"price": 9.99, "store_id": 1}),
                                      headers={'Content-Type': 'application/json'})

                self.assertEqual(response.status_code, 200)
                self.assertIsNotNone(ItemModel.find_by_name('test item'))
                self.assertEqual(ItemModel.find_by_name('test item').price, 9.99)

    def test_item_list(self):
        with self.app() as client:
            with self.app_context():
                StoreModel('test store').save_to_db()
                ItemModel('test item', 7.99, 1).save_to_db()
                response = client.get('/items')

                self.assertDictEqual(json.loads(response.data),
                                     {'items': [{'name': 'test item',
                                                'price': 7.99}]})

