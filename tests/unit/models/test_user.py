from models.user import UserModel
from tests.unit.unit_base_test import UnitBaseTest


class UserTest(UnitBaseTest):
    def test_create_user(self):
        user = UserModel('test user', 'passwd')

        # Recommendation: Always add custom error messages!
        self.assertEqual(user.username, 'test user')
        self.assertEqual(user.password, 'passwd')
