from db import db


class ItemModel(db.Model):
    __tablename__ = 'items'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    price = db.Column(db.Float(precision=2))

    # initialize new column in items table and set relation to primary key of target relation
    store_id = db.Column(db.Integer, db.ForeignKey('stores.id'))
    # NOTE: column of related tables is initialized and will be mapped against primary key of store. Hence, all
    # columns from StoreModel will be available in ItemModel as i.e. "item.store.name"
    store = db.relationship('StoreModel')

    def __init__(self, name, price, store_id):
        self.name = name
        self.price = price
        self.store_id = store_id

    def json(self):
        return {'name': self.name, 'price': self.price}

    @classmethod
    def find_by_name(cls, name):
        return cls.query.filter_by(name=name).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
